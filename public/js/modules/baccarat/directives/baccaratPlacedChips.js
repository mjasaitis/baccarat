baccaratModule.directive("baccaratPlacedChips", [
    "$window",
    "$timeout",
    function(
        $window,
        $timeout
    ) {
        return {
            restrict: "A",
            templateUrl: "js/modules/baccarat/views/placedChips.html",
            scope: {
                chipsStack: "=",
            },
            transclude: true,
            link: function($scope, elem, attrs) {
                "use strict";

                $scope.$watch('chipsStack', function(newValue, oldValue) {

                    $scope.stackValue = 0;

                    if (angular.isDefined($scope.chipsStack)) {
                        angular.forEach($scope.chipsStack, function(chip) {
                            $scope.stackValue += chip;
                        })
                    }
                });
            }
        };
    }
]);