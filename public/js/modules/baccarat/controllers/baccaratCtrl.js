baccaratModule.controller("baccaratCtrl", [
    "$scope",
    "$state",
    "settings",
    "$timeout",
    "$window",
    "baccaratService",
    "socket",
    function(
        $scope,
        $state,
        settings,
        $timeout,
        $window,
        baccaratService,
        socket
    ) {
        $scope.loading = true;
        $scope.cardsDealing = false;
        $scope.bankerCards = [];
        $scope.playerCards = [];
        $scope.winner = {};
        $scope.errors = {};
        $scope.selectedChipValue = 0;
        $scope.userChips = 0;
        $scope.chipValues = [50000, 10000, 5000, 1000, 500];
        $scope.betLimits = {
            from: 500,
            to: 200000,
            text: "500 - 200K"
        };

        $scope.gameNumber = "AC07A170FD";

        $scope.bet = {
            canBePlaced: false,
            placed: false,
            //type: ""
        };

        $scope.placedBets = {};
        $scope.imagesLoaded = false;

        $scope.preloadImages = function() {

            var images = [
                "img/cards.png",
                "img/chips_placed.png",
                "img/chips.png",
                "img/game_area.png"
            ];

            var finishedCnt = 0;

            angular.forEach(images, function(imgSrc) {
                var img = new Image();
                img.src = imgSrc;

                img.onload = function() {
                    finishedCnt++;
                    if (finishedCnt == images.length) {
                        $scope.imagesLoaded = true;
                    }
                };
                img.onerror = function() {
                    if (finishedCnt == images.length) {
                        $scope.imagesLoaded = true;
                    }
                }
            });

        }();

        socket.on('connect_error', function() {
            console.log('Connection Failed');
            $scope.info = {
                show: true,
                text: "Connection error"
            }
        });


        socket.on('reconnect', function() {
            $scope.info.show = false;
        });

        socket.on('gameStatus', function(data) {
            console.log("data", data);

            if ($scope.loading) {
                $timeout(function() {
                    $scope.loading = false;
                }, 500);
            }

            $scope.bet.canBePlaced = data.canPlaceBet;
            $scope.bankerCards = data.bankerCards;
            $scope.playerCards = data.playerCards;
            $scope.userChips = data.userChips;

            $scope.winner = {};

            if (angular.isDefined(data.betPlaceTime)) {
                if ($scope.bet.canBePlaced === true) {
                    $scope.bet.placed = false;
                    $scope.countDown(data.betPlaceTime);
                }
            }

            if ($scope.bet.canBePlaced === true) {
                $scope.bet.placed = false;
                $scope.placedBets = {};
                $scope.bet.chipsStacks = {};

                $scope.info = {
                    show: true,
                    text: "已开局，请下注" // please place bets
                }

                $timeout(function() {
                    $scope.info.show = false;
                }, 1500);


            } else {
                //$scope.bet.type = "";
                $scope.playerPoints = $scope.calculatePoints($scope.playerCards);
                $scope.bankerPoints = $scope.calculatePoints($scope.bankerCards);

                if (!$scope.bet.placed) {
                    $scope.bet.chipsStacks = {};
                }
            }

            if (angular.isDefined(data.winner)) {

                $timeout(function() {
                    $scope.winner.show = true;
                    $scope.winner.type = data.winner;
                }, 500);

                if (data.winner == "tie") {
                    $scope.winner.tie = true;
                } else if (data.winner == "player") {
                    $scope.winner.cards = data.playerCards;
                } else {
                    $scope.winner.cards = data.bankerCards;
                }

            } else {
                $scope.winner.show = false;
            }
        });

        $scope.selectChip = function($event, chipValue) {
            $scope.selectedChipValue = chipValue;
        };

        $scope.cardValue = function(card) {

            var value = card.substring(1);
            if (value == "a") {
                return 1;
            }

            value = parseInt(value);
            if (isNaN(value)) {
                return 0;
            } else {
                return value;
            }

        };

        $scope.calculatePoints = function(cards) {
            var points = 0;
            for (var i = 0; i < cards.length; i++) {
                var card = cards[i];
                points += $scope.cardValue(card);
            };

            points = points % 10;
            return points;
        };

        $scope.placeBet = function(bet) {

            if (!$scope.bet.canBePlaced || $scope.selectedChipValue === 0) {
                return;
            }

            if (!angular.isDefined($scope.bet.chips)) {
                $scope.bet.chips = 0;
            }

            if ($scope.bet.chips + $scope.selectedChipValue > $scope.userChips) {
                return;
            }

            if ($scope.bet.chips + $scope.selectedChipValue > $scope.betLimits.to) {
                return;
            }

            $scope.bet.chips += $scope.selectedChipValue;
            $scope.userChips -= $scope.selectedChipValue;

            if (!angular.isDefined($scope.placedBets[bet])) {
                $scope.placedBets[bet] = 0;
            }

            $scope.placedBets[bet] += $scope.selectedChipValue;
            $scope.bet.chipsStacks[bet] = $scope.getStackChips($scope.placedBets[bet]);

            console.log("Placed best", $scope.placedBets);
            console.log("stacks", $scope.bet.chipsStacks);
        };

        $scope.getStackChips = function(value) {

            var wholeNumber;
            var stack = [];

            angular.forEach($scope.chipValues, function(chipValue) {

                if (value >= chipValue) {
                    wholeNumber = Math.floor(value / chipValue);
                    value -= wholeNumber * chipValue;

                    for (var i = 0; i < wholeNumber; i++) {
                        stack.push(chipValue);
                    }
                }
            });

            return stack;

        };

        $scope.countDown = function(betPlaceTime) {
            if (betPlaceTime <= -1) {
                $scope.bet.canBePlaced = false;
                return;
            }

            var seconds = angular.copy(betPlaceTime);
            var minutes = Math.floor(seconds / 60);
            seconds = seconds - minutes * 60;
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }

            $scope.betPlaceTimeFormated = minutes + ":" + seconds;

            $timeout(function() {
                betPlaceTime--;
                $scope.countDown(betPlaceTime);
            }, 1000);
        }

        $scope.confirmBet = function() {
            if (!$scope.bet.canBePlaced || $scope.bet.placed || !angular.isDefined($scope.bet.chips) || $scope.bet.chips === 0) {
                return;
            }

            $scope.bet.placed = true;
            $scope.info = {
                show: true,
                text: "下注成功" //  "Bets have been placed successfullly"
            }

            $timeout(function() {
                $scope.info.show = false;
            }, 1500);

        };

        $scope.cancelBet = function() {
            if (!$scope.bet.canBePlaced && !$scope.bet.placed) {
                return;
            }

            $scope.userChips += $scope.bet.chips;
            $scope.placedBets = {};
            $scope.bet.chipsStacks = {};
            $scope.bet.chips = 0;
        };

    }
]);