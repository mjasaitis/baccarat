var baccaratModule = angular.module("baccaratModule", []);

// Global settings for particular module
baccaratModule.constant("moduleSettings", {
    rootPath: "js/modules/baccarat"
});

baccaratModule.config(["$stateProvider", "$urlRouterProvider", "moduleSettings", 
    function($stateProvider, $urlRouterProvider, moduleSettings) {

        $stateProvider
            .state("index", {
                url: "/",
                views: {
                    "content": {
                        templateUrl: moduleSettings.rootPath + "/views/frontpage.html",
                        controller: "baccaratCtrl"
                    }
                }
            });
    }
]);