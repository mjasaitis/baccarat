"use strict";

document.addEventListener('gesturestart', function(e) {
    e.preventDefault();
});

document.addEventListener('touchmove', function(event) {
    event = event.originalEvent || event;
    if (event.scale !== 1) {
        event.preventDefault();
    }
}, false);

var appBaccarat = angular.module("app-baccarat", [
    // Third party modules
    "ui.router",
    "ngAnimate",
    // Modules
    "baccaratModule"
]);

appBaccarat.config(["$stateProvider", "$urlRouterProvider",
    function($stateProvider, $urlRouterProvider) {
        // Redirect to index state from any invalid state
        $urlRouterProvider.otherwise("/");
    }
]);

appBaccarat.factory('socket', ["$rootScope", "settings", function($rootScope, settings) {
    var socket = io(settings.gameEngineUrl);

    console.log(settings);

    return {
        on: function(eventName, callback) {
            socket.on(eventName, function() {
                var args = arguments;
                $rootScope.$apply(function() {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function(eventName, data, callback) {
            socket.emit(eventName, data, function() {
                var args = arguments;
                $rootScope.$apply(function() {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        }
    };
}]);