module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        frontendCssSource: "scss",
        frontendCssTarget: "public/css",

        cssmin: {
            expand: false,

            frontend_css: {
                src: '<%= frontendCssTarget %>/main.css',
                dest: '<%= frontendCssTarget %>/main.min.css'
            }

        },

        sass: {
            options: {
                sourceMap: true,
            },
            frontend_css: {

                files: {
                    '<%= frontendCssTarget %>/main.css': '<%= frontendCssSource %>/main.scss'
                }
            }
        },

        watch: {
            frontend_css: {
                files: ['<%= frontendCssSource %>/**/*.scss'],
                tasks: [
                    'sass:frontend_css',
                    'cssmin:frontend_css'
                ]
            }
        },

        injector: {
            production_settings: {
                options: {
                    starttag: '<!-- injector:js-settings -->',
                    endtag: '<!-- endinjector -->',
                    ignorePath: 'public/'
                },
                files: {
                    'public/index.html': "public/js/settings.prod.js"
                }
            },
            local_settings: {
                options: {
                    starttag: '<!-- injector:js-settings -->',
                    endtag: '<!-- endinjector -->',
                    ignorePath: 'public/'
                },
                files: {
                    'public/index.html': "public/js/settings.local.js"
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-injector');

    grunt.registerTask('default', ['watch']);
    
    grunt.registerTask('build_production', [
        "injector:production_settings",
        "sass:frontend_css",
        "cssmin:frontend_css"
    ]);

    grunt.registerTask('build_local', [
        "injector:local_settings",
        "sass:frontend_css",
        "cssmin:frontend_css"
    ]);
    
};