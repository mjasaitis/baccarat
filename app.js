var express = require('express');
var app = module.exports = express.createServer();

app.configure('development', function() {
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function() {
    app.use(express.errorHandler());
});


// app.use(express.static('public'));

app.use('/', express.static('public'));

app.use(express.static('bower_components'));
app.use(express.static('bower_components_manual'));


var port = Number(process.env.PORT || 3001)

// Start server 
app.listen(port, function() {
    console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});